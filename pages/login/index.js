import { useState, useContext, useEffect } from 'react'; 
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import UserContext from '../../UserContext';
import users from '../../data/users'; 
import Router from 'next/router';
import Head from 'next/head';
import Link from 'next/link';
import View from '../../components/View';
import { GoogleLogin } from 'react-google-login';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';

export default () => {
        return(
            <View title={'Login'}>
                <Row className="justify-content-center">
                    <Col xs md="6">
                        <h3></h3>
                        <LoginForm />
                    </Col>
                </Row>
            </View>   
        )
}
    const LoginForm = () => {
    const { user, setUser } = useContext(UserContext)

    const [ email, setEmail] = useState('');
    const [ password, setPassword] = useState('');
    const [ tokenId, setTokenId ] = useState(null)

    function authenticate(e){
        e.preventDefault() 

        const options = {   
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({ email: email, password: password })
        }

        fetch(`${ AppHelper.API_URL }/users/login`, options).then(AppHelper.toJSON).then(data => {
            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error === 'does-not-exist'){
                    Swal.fire('Authentication Failed', 'User does not exist', 'error')
                } else if(data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is Incorrect', 'error')
                } else if(data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try using an alternative login procedure', 'error')
                }
            }
        })
    }

    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${accessToken}`}
        } 
        fetch(`${AppHelper.API_URL}/users/details`, options).then(AppHelper.toJSON).then(data => {

            setUser({ id: data._id, isAdmin: data.isAdmin })
            Router.push('/transactions')
        })
    }

    const authenticateGoogleToken = (response) => {
        console.log(response) 
        localStorage.setItem('googleToken', response.accessToken)
        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload).then(AppHelper.toJSON).then(data => {
                if(typeof data.accessToken !== 'undefined'){
                    localStorage.setItem('token', data.accessToken)
                    retrieveUserDetails(data.accessToken)
                } else {
                    if(data.error === 'google-auth-error'){ 
                        Swal.fire('Google Auth Error', 'Google Authentication failed', 'error')
                    } else if(data.error === 'login-type-error') {
                        Swal.fire('Login Type Error', 'You may have registered through a different login procedure', 'error')
                    }
                  }  
                }
            )
        } 

        return(
              <Card>
                <Card.Header>
                    <img src="/loginImg.png" className="loginImg" alt="Login" />
                </Card.Header>
                <Card.Body>
                    <Form onSubmit={ authenticate }>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)} autoComplete="off" required/>
                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                        </Form.Group>
                        <Button className="bg-success mb-2" type="submit" block> Login </Button>
                        <GoogleLogin 
                           clientId="985519934370-5i8us3m45hhri4lae8fvu0edcc0glbo4.apps.googleusercontent.com"
                           buttonText="Login using Gmail"
                           onSuccess={ authenticateGoogleToken }
                           onFailure={ authenticateGoogleToken }
                           cookiePolicy={ 'single_host_origin' }
                           className="w-100 text-center d-flex justify-content-center"
                        />
                        <br />
                        <Link href="/register">
                         <a className="nav-link text-center" role="button">No Account? Register here!</a>             
                        </Link>
                    </Form>
                </Card.Body>
              </Card>    
            )
    }
