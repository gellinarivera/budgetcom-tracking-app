import { useEffect } from 'react'
import UserContext from '../../UserContext'
import Router from 'next/router'

export default function logout() {
    useEffect(() => {
        Router.push('/login')
    },[])
    return null
}