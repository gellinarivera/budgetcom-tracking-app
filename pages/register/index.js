import { useState, useEffect } from 'react'; 
import Head from 'next/head';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import Router from 'next/router';

export default function index(){
     const [ formData, setFormData ] = useState({
        firstName: "", 
        lastName: "", 
        email: "", 
        password: "",
        confirmPassword: ""
     }) 

     const { firstName, lastName, email, password, confirmPassword } = formData

     const onChangeHandler = e => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
     }
     const [isActive, setIsActive ] = useState(false)

     useEffect(() => {

     
     }, )

     function registerUser(e){
        e.preventDefault()
        if((firstName !== ''&& lastName !== '' && email !== '' && password !== ''&&  confirmPassword !== '')&&(confirmPassword === password)){
            fetch('http://localhost:4000/api/users/email-exists', {
                method: 'POST', 
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email
                }) 
            })
            .then(res => res.json())
            .then(data => { 
                if(data === false){
                   fetch('http://localhost:4000/api/users', {
                      method: 'POST', 
                      headers: {
                        'Content-Type': 'application/json'
                      }, 
                      body: JSON.stringify(formData)
                   })
                   .then(res => {
                       return res.json()
                   })
                   .then(data => {
                       console.log(data)
                       if(data === true){
                        setFormData({
                            firstName: "", 
                            lastName: "", 
                            email: "", 
                            password: "",
                            confirmPassword: ""
                        })
                        Swal.fire({
                            icon: 'success', 
                            title: 'New Account Registered Successfully', 
                            text: 'You may Now Log in'
                        })
                        Router.push('/login')
                       } else {
                           Swal.fire({
                            icon: 'warning', 
                            title: 'Something Went Wrong, Check Credentials!'
                           })
                       }
                   })
                }
            })
        }else {
            Swal.fire('Something Went wrong!')
        }
       
     }
  return(
        <>
           <Head>
               <title> Register </title>
           </Head>
           <div className="registerContainer">
           <Form onSubmit={(e) => registerUser(e)}>
            <div className="form-group">
                <input type="text" id="firstName" name="firstName" className="form-control" value={firstName} placeholder="Insert First Name Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
            </div>
            <div className="form-group">
                <input type="text" id="lastName" name="lastName" className="form-control" value={lastName} placeholder="Insert Last Name Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
            </div>
            <div className="form-group">
                <input type="email" id="email" name="email" className="form-control" value={email} placeholder="Insert Email Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
            </div>
            <div className="form-group">
                <input type="password" id="password" name="password" className="form-control" value={password} placeholder="Insert Password Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
            </div>
            <div className="form-group">
                <input type="password" id="confirmPassword" name="confirmPassword" className="form-control" value={confirmPassword} placeholder="Confirm Password Here" autoComplete="off" onChange={e => onChangeHandler(e)}/>
            </div>
            <button type="submit" className="btn btn-success">Register</button>
            </Form> 
            </div>      
      </> 
    )
} 