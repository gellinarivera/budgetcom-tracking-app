import { useState, useEffect, useContext } from 'react'
import UserContext from '../../UserContext'
import { Jumbotron, Container, Image, Row, Col } from 'react-bootstrap'
import AddRecord from '../../components/AddRecord'
import RecordList from '../../components/RecordList'
import Head from 'next/head'

export default function index() {
    const { user } = useContext(UserContext)
    const [ name, setName ] = useState('')
    const [ categories, setCategories ] = useState([])
    const [ records, setRecords ] = useState([])
    const [ incomeSum, setIncomeSum ] = useState([])
    const [ expenseSum, setExpenseSum ] = useState([])

    let balanceArr = records.map(record => record.amount),sum
    balanceArr = balanceArr.map(recordAmount => sum = (sum || 0) + recordAmount)

    const currentBalance = balanceArr.slice(-1)

    const incomeStyle = {color: "#588E29"}
    const totalIncomeStyle = {color: "#588E29"}
    const expenseStyle = {color: "#C8040E"}
    const totalExpenseStyle = {color: "#C8040E"}
    const amountStyle = (currentBalance >= 0) ? incomeStyle : expenseStyle

    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {
            setName(data.firstName)
            setRecords(data.transactions)
            setCategories(data.categories)

            const incomeCalc = data.transactions
                .filter(record => record.categoryType === 'Income')
                .map(record => record.amount)
                .reduce((total, amount) => total + amount, 0)
            setIncomeSum(incomeCalc)

            const expenseCalc = data.transactions
                .filter(record => record.categoryType === 'Expense')
                .map(record => record.amount)
                .reduce((total, amount) => total + amount, 0)
            setExpenseSum(expenseCalc)
        })
    },[])

    return (
        <>
        <Head><title>Transactions</title></Head>
        {
        (user.id === null)
        ?   <Jumbotron className="recordJumbotron mt-5">
                </Jumbotron>
        :   <>
            {categories.length === 0 && records.length === 0
            ?   <Jumbotron className="recordJumbotron mt-5">
                    <Row>
                        <Col className="recordInfo">
                            <h1>Welcome, {name}!</h1>
                            <h3>Your Current Balance is: PHP 0</h3>
                            <h5>Total Income: PHP <span style={totalIncomeStyle}>{incomeSum}</span></h5>
                            <h5>Total Expenses: PHP <span style={totalExpenseStyle}>{Math.abs(expenseSum)}</span></h5>
                            <br/><br/>
                            <h6 style={{color:"red"}}>NO DATA YET? Please go create your first category at "Categories"!</h6>
                        </Col>
                    </Row>
                </Jumbotron>
            :   <>
                <Jumbotron className="recordJumbotron mt-5">
                    <Row>
                        <Col className="recordInfo">
                            <h1>Welcome, {name}!</h1>
                            <h3>Your Current Balance is: PHP <span style={amountStyle}>{currentBalance}</span></h3>
                            <h5>Total Income: PHP <span style={totalIncomeStyle}>{incomeSum}</span></h5>
                            <h5>Total Expenses: PHP <span style={totalExpenseStyle}>{Math.abs(expenseSum)}</span></h5>
                        </Col>
                    </Row>
                </Jumbotron>
                <AddRecord categories={categories} setRecords={setRecords} />
                {(records.length === 0)
                ?   <Container className="error-container" fluid>
                    </Container>
                :   <RecordList records={records} categories={categories} setRecords={setRecords} incomeStyle={incomeStyle} expenseStyle={expenseStyle} />
                }
                </>
            }
            </>
        }
        </>
    )
}