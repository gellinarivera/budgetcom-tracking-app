import { Line } from 'react-chartjs-2'

export default function LineChart({records}) {
    const chartDates = records.map(record => new Date(record.dateAdded).toLocaleString('en-US',{
        month:'short',
        day:'numeric',
        hour: '2-digit'
    }))
    .sort((a,b) => b - a)
    
    let chartAmounts = records.map(record => record.amount),sum
    chartAmounts = chartAmounts.map(recordAmount => sum = (sum || 0) + recordAmount)

    return (
        <div className="col-md-12 pt-5">
        <Line
            data={{
                datasets:[{
                    label: 'Balance',
                    fill: false,
                    data: chartAmounts,
                    backgroundColor: '#FF1FA9',
                    pointBorderColor: 'yellow',
                    borderColor: 'green'
                }],
                labels: chartDates
            }}

            redraw={false}
        />
        </div>
    )
}