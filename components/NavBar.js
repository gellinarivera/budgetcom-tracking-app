import { useContext } from 'react'; 
import Navbars from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav'; 
import Link from 'next/link';
import UserContext from '../UserContext'; 

export default function Navbar(){ 
  const { user } = useContext(UserContext); 
  return (
          <Navbars bg="light" expand="lg">
          <Link href="/">
             <a className="navbar-brand">B U D G E T . C O M</a>
          </Link>  
          <Navbars.Toggle aria-controls="basic-navbar-nav"/>  
          <Navbars.Collapse id="basic-navbar-nav">
            {(user.email === null)
                ?   null
                :   <Nav className="ml-auto">
                    <Link href="/transactions">
                        <a className="nav-link" role="button">Transactions</a>
                    </Link>
                    <Link href="/categories">
                        <a className="nav-link" role="button">Categories</a>
                    </Link>
                    <Link href="/trends">
                        <a className="nav-link" role="button">Trends</a>
                    </Link>
                    <Link href="/breakdown">
                        <a className="nav-link" role="button">Breakdown</a>
                    </Link>
                    <Link href="/logout">
                        <a className="nav-link" id="logout" role="button">Logout</a>
                    </Link>
                    </Nav>
            }
          </Navbars.Collapse>         
          </Navbars>   
    )
}